# cloudlog

This project is docker image of https://github.com/magicbug/Cloudlog

Inspired by https://hub.docker.com/r/waarlandit/cloudlog

**Note: it looks like some installation scripts doesn't work properly if base was created before running install**

## Start with Docker Compose 2

```yaml
version: "2.4"
services:
  mariadb:
    image: mariadb:10
    container_name: mariadb
    mem_limit: 256M
    environment:
      MARIADB_ROOT_PASSWORD: ${MARIADB_ROOT_PASSWORD}
      MARIADB_DATABASE: cloudlog
      MARIADB_USER: cloudlog
      MARIADB_PASSWORD: CHANGE_ME_PASSWORD
    expose: 
      - "3306"
    restart: unless-stopped
    volumes:
      - "mariadb-db:/var/lib/mysql"
  cloudlog:
    image: registry.gitlab.com/p8150/cloudlog
    container_name: cloudlog
    mem_limit: 128M
    depends_on:
      - mariadb
    ports:
      - "8080:80"
    restart: unless-stopped
    volumes:
      - cloudlog-config:/opt/cloudlog/application/config
      - cloudlog-backup:/opt/cloudlog/application/backup
      - cloudlog-upload:/opt/cloudlog/application/uploads
volumes:
  cloudlog-config:
  cloudlog-backup:
  cloudlog-upload:
  mariadb-db:
```

## Start with Docker Compose 3

```yaml
version: "3.8"
services:
  cloudlog-db:
    container_name: cloudlog-db
    deploy:
      restart_policy: 
        condition: any
      resources:
        limits:
          memory: 256M
    environment:
      - MARIADB_ROOT_PASSWORD=${CLOUDLOG_MARIADB_ROOT_PASSWORD}
    image: mariadb:10.7
    ports: 
      - "3306:3306"
    volumes:
      - "cloudlog-db:/var/lib/mysql"
  cloudlog:
    container_name: cloudlog
    depends_on:
      - cloudlog-db
    deploy:
      restart_policy: 
        condition: any
      resources:
        limits:
          memory: 128M
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:80"]
      interval: 1m30s
      timeout: 30s
      retries: 5
      start_period: 30s
    image: registry.gitlab.com/p8150/docker-images/cloudlog
    ports:
      - "8080:80"
    volumes:
      - cloudlog-config:/opt/cloudlog/application/config
      - cloudlog-backup:/opt/cloudlog/application/backup
      - cloudlog-upload:/opt/cloudlog/application/uploads
volumes:
  cloudlog-config:
  cloudlog-backup:
  cloudlog-upload:
  cloudlog-db:
```