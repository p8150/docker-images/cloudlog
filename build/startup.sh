#!/bin/sh
DIR="/opt/cloudlog"
cd ${DIR} && git pull

echo "curl --silent http://localhost/index.php/lotw/load_users &>/dev/null" > /etc/periodic/weekly/cloudlog_load_users
echo "curl --silent http://localhost/index.php/update/update_clublog_scp &>/dev/null" > /etc/periodic/weekly/update_cloudlog_scp
echo "${DIR}/update_cloudlog.sh &>/dev/null" > /etc/periodic/weekly/cloudlog_update

chown lighttpd.root ${DIR} -R
chmod -R g+rw ${DIR}/uploads/
chmod -R g+rw ${DIR}/updates/
chmod -R g+rw ${DIR}/backup/
chmod -R g+rw ${DIR}/application/config/
chmod -R g+rw ${DIR}/assets/qslcard/
chmod +x ${DIR}/update_cloudlog.sh
sed -i -e 's/root:www-data/lighttpd.lighttpd/' ${DIR}/update_cloudlog.sh

lighttpd -f /etc/lighttpd/lighttpd.conf -D 